
    function hideshow(which, toggleText){

        if (!document.getElementById)
            return
        if (which.style.display=="block") {
            which.style.display="none"
            toggleText.innerHTML="Show"
        } else {
            which.style.display="block"
            toggleText.innerHTML="Hide"
        }
    }

    get_form_specific_fields = function(form, form_specific_fields) {

        var specific_form_fields = form_specific_fields[form.attr("id")];

        var array_of_specific_fields = [];

        for (var i = 0; i < specific_form_fields.length; i++) {
            var id = specific_form_fields[i];
            array_of_specific_fields[i] = form.find('#'+id);
        }

        return array_of_specific_fields;

    }

	validate = function(){

        //key is form id, value is an array of inputs with id of
        var form_specific_fields = {

            "form1" : ["form1SpecificThing1", "form1SpecificThing2"],
            "form2" : ["form2SpecificThing1"],
            "form3" : ["form3SpecificThing1", "form3SpecificThing2", "form3SpecificThing3"]

        };

           var form = $(this).parents('form');

           // clear message div
           form.find( "#msg" ).html( "" );

           // get values for all input boxes
           var userName = form.find( 'input[name="userName"]' ).val();
           var email = form.find( 'input[name="email"]' ).val();
           var pass1 = form.find( 'input[name="password"]' ).val();
           var pass2 = form.find( 'input[name="chkPassword"]' ).val();


 
           // no empty values permitted
           var hasValue = userName && email && pass1 && pass2;
           if( !hasValue ){
               form.find( "#msg" )
               .append( "All Fields are required." )
               .css( "color","red" );
               return false;
           }

            //no empty form specific values permitted
            var array_of_specific_fields = get_form_specific_fields(form, form_specific_fields);

            for (var i = 0; i < array_of_specific_fields.length; i++) {

                hasValue = array_of_specific_fields[i].val();
                if( !hasValue ){
                    form.find( "#msg" )
                        .append( "All Fields are required." )
                        .css( "color","red" );
                    return false;
                }

            }

           // check that passwords match
           var passwordMatch = false;
           if( pass1 === pass2 ) {
               passwordMatch = true;
           }
 
           if( !passwordMatch ){
               form.find("#msg").append("<p>Passwords don't match. </p>").css("color", "red");
               return false;
           } 
    }

	addLocation = function(){

            //debugger;

            var form = $(this).parents('form');

        $("<select name='stateCombo' id='stateCombo'><option>"
            + "Select State</option></select>").insertAfter(form.find("input[name=addLocation]"));


            // disable add location button so that we don't get
            // more than one drop-down
            $(this).attr("disabled", "disabled");
 
            // add some sample states
            var states = ["California", "Florida", "New York"];
            $.each(states, function(index, value){
                   form.find("[name='stateCombo']").append("<option value='"
                                                   + index
                                                   + "'>" 
                                                   + value 
                                                   + "</option>");
            });
 
            // add another empty select list

        $("<select name='cityCombo'>"
            + "<option>Select City</option></select>").insertAfter(form.find("select[name=stateCombo]"));

    }

	other = function(event){

        var form = $(this).parents('form');

        var selectedState = $(this).val();

        // get name of state and fill with some data
        var CA_Cities = ["San Francisco", "Los Angeles", "Mountain View"];
        var FL_Cities = ["Fort Lauderdale", "Miami", "Orlando"];
        var NY_Cities = ["New York", "Buffalo", "Ithica"];
            var cities = [];
 
            if(selectedState == 0){
               cities = form.extend([], CA_Cities);
            } else if(selectedState == 1){
               cities = form.extend([], FL_Cities);
            } else if(selectedState == 2){
               cities = form.extend([],NY_Cities);
            }
 
            // clear cityCombo of any previous values
            form.find("[name='cityCombo']").empty();

            $.each(cities, function(index, value){

                form.find("[name='cityCombo']").append("<option value='"
                                               +index
                                               +"'>"
                                               +value
                                               +"</option>"); 

            });
    }

